#ifndef __APMONITOR_REPEATINGEVENT_H
#define __APMONITOR_REPEATINGEVENT_H

#include "PollingTimer.h"

class RepeatingEvent {
public:
    RepeatingEvent(void (*function)(void), const unsigned long durationMillis);
    ~RepeatingEvent();
    void Process();
    void Reset();

private:
    PollingTimer *_timer;
    void (*_function)(void);
};

#endif
