[CmdletBinding()]
param()

$ErrorActionPreference = "Stop"
Set-StrictMode -Version 5.0

particle subscribe apsystem mine | foreach {

    $line = $_
    try {
        ConvertFrom-Json $line
    }
    catch {
        Write-Host $line
        return;
    }

    Write-Host "----------------------------------------"

} | Format-List -Property name, published_at, data