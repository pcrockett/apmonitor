#include "ParticleConnection.h"
#include "application.h"

ParticleConnection::ParticleConnection(ParticleEventPublisher *eventPublisher) {
    _eventPublisher = eventPublisher;
}

void ParticleConnection::Process() {

    bool wifiConnected = WiFi.ready();
    bool particleConnected = Particle.connected();

    if (!wifiConnected && !WiFi.connecting()) {
        WiFi.connect();
    }
    else if (wifiConnected && !particleConnected) {
        Particle.connect();
    }
    else if (!_isConnected && wifiConnected && particleConnected) {
        _eventPublisher->Enqueue("connection", "connected");
        _isConnected = true;
    }
    else if (_isConnected && (!wifiConnected || !particleConnected)) {

        // This event won't get published immediately, but when it does
        // eventually get published, you'll be able to see exactly when we
        // became disconnected.
        _eventPublisher->Enqueue("connection", "disconnected");
        _isConnected = false;
    }

}

bool ParticleConnection::IsConnected() {
    return _isConnected;
}
