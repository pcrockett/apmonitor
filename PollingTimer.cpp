#include "PollingTimer.h"

PollingTimer::PollingTimer(const unsigned long durationMillis) {
    _durationMillis = durationMillis;
    _lastResetTime = 0;
}

bool PollingTimer::HasElapsed() {
    return (millis() - _lastResetTime) >= _durationMillis;
}

void PollingTimer::Reset() {
    _lastResetTime = millis();
}
