#ifndef __APMONITOR_LIGHTSCHEDULER_H
#define __APMONITOR_LIGHTSCHEDULER_H

#include "GrowLight.h"

const unsigned int LightStartHour = 6; // 6:00 AM
const unsigned int LightStopHour = 21; // 9:00 PM

class LightScheduler {
public:
    LightScheduler(GrowLight* lights);
    void Process();

private:
    LightState GetScheduledState();
    GrowLight* _lights;

    // _currentStatus may get out of sync with _lights->GetStatus(). This is
    // intentional. _currentStatus keeps track of what the LightScheduler
    // WANTS. It is NOT for tracking what is actually happening in reality.
    LightState _currentStatus;
};

#endif
