#include "WaterLevelSensor.h"

WaterLevelSensor::WaterLevelSensor(const unsigned int sensorPin) {
    _pin = sensorPin;
}

void WaterLevelSensor::Init() {
    pinMode(_pin, INPUT);
}

WaterLevel WaterLevelSensor::Read() {

    int prevValue = digitalRead(_pin);
    int retryCount = 0;

    for (int i = 0; i < 5; i++) {
        delay(10);

        int value = digitalRead(_pin);
        if (value != prevValue) {

            if (++retryCount > 10) {
                return Unknown;
            }

            i = 0;
            prevValue = value;
        }
    }

    if (prevValue == HIGH) {
        return AboveThreshold;
    }
    else {
        return BelowThreshold;
    }
}
