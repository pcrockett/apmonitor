#ifndef __APMONITOR_PARTICLECONNECTION_H
#define __APMONITOR_PARTICLECONNECTION_H

#include "ParticleEventPublisher.h"
#include "PollingTimer.h"

class ParticleConnection {
public:
    ParticleConnection(ParticleEventPublisher *eventPublisher);
    void Process();
    bool IsConnected();

private:
    ParticleEventPublisher* _eventPublisher;
    bool _isConnected = false;

};

#endif
