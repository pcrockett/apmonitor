#include "GrowLight.h"

GrowLight::GrowLight(const unsigned int relayPin, ParticleEventPublisher *eventPub) {
    _relayPin = relayPin;
    _currentLightState = On; // Default value is enforced by hardware; The Photon has to explicitly turn it off.
    _desiredLightState = On;
    _eventPub = eventPub;
}

void GrowLight::Init() {
    pinMode(_relayPin, OUTPUT);
}

void GrowLight::TurnOn() {
    _desiredLightState = On;
    _eventPub->Enqueue("lights/requested", "On");
}

void GrowLight::TurnOff() {
    _desiredLightState = Off;
    _eventPub->Enqueue("lights/requested", "Off");
}

LightState GrowLight::GetStatus() {
    return _currentLightState;
}

LightState GrowLight::Process() {

    if (_currentLightState == _desiredLightState || !_changeDelay.HasElapsed()) {
        return _currentLightState;
    }

    if (_desiredLightState == On) {
        digitalWrite(_relayPin, LOW);
        _currentLightState = On;
        _eventPub->Enqueue("lights/changed", "On");
    }
    else if (_desiredLightState == Off) {
        digitalWrite(_relayPin, HIGH);
        _currentLightState = Off;
        _eventPub->Enqueue("lights/changed", "Off");
    }
    else {
        // Should never happen
    }

    _changeDelay.Reset();

    return _currentLightState;
}
