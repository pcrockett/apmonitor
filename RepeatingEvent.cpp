#include "RepeatingEvent.h"

RepeatingEvent::RepeatingEvent(void (*function)(void),
    const unsigned long durationMillis) {

    _timer = new PollingTimer(durationMillis);
    _function = function;
}

RepeatingEvent::~RepeatingEvent() {
    delete _timer;
}

void RepeatingEvent::Process() {
    if (_timer->HasElapsed()) {
        _function();
        _timer->Reset();
    }
}

void RepeatingEvent::Reset() {
    _timer->Reset();
}
