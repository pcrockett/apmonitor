#ifndef __APMONITOR_PARTICLE_EVENT_PUBLISHER_H
#define __APMONITOR_PARTICLE_EVENT_PUBLISHER_H

#include "application.h"
#include "PollingTimer.h"
#include <queue>

// Particle limits us to 1 event per second. Thingspeak limits us to 1 update
// per 15 seconds. We'll update every 16 seconds to be safe.
const unsigned int MinPublishDelayMillis = 16000;

// Just to make sure we don't use up too many resources...
const unsigned int MaxEventQueueSize = 200;

class EventInstance {
public:
    EventInstance(String eventName, String data);
    String GetEventName();
    String GetEventData();

private:
    String _eventName;
    String _data;
};

class ParticleEventPublisher {
public:
    ParticleEventPublisher();
    ParticleEventPublisher(String eventPrefix);
    void Enqueue(String eventName, String data);
    void Process();
    size_t GetQueueLength();

private:
    PollingTimer _publishDelay = PollingTimer(MinPublishDelayMillis);
    std::queue<EventInstance> _eventQueue;
    String _eventPrefix;
};

#endif
