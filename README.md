# APMonitor

I have an [aquaponics](https://en.wikipedia.org/wiki/Aquaponics) system in my garage. I am using a [Particle Photon](https://www.particle.io/products/hardware/photon-wifi-dev-kit) to control and monitor the system. This is the code that's running on the Photon.

My [APSystem project](https://bitbucket.org/pcrockett/apsystem) contains the PowerShell code that allows me to remotely monitor and control the system from my laptop.

For those who are unfamiliar with Arduino / Particle sketches, `apmonitor.ino` is the entry point to the whole program.

To-do list (if I ever find the time):

 - Add water temperature sensor
 - Add relays to control the pump and water heater
 - Upgrade existing relay to handle more grow lights