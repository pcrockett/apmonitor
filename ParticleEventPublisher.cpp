#include "ParticleEventPublisher.h"

EventInstance::EventInstance(String eventName, String data) {
    _eventName = eventName;

    time_t currentTime = Time.now();
    String fullTimeStr;
    
    if (Time.year(currentTime) < 2016) {
        // A sync with the cloud hasn't happened yet. We don't know what time it is.
        fullTimeStr = "";
    }
    else {
        fullTimeStr = Time.format(currentTime, TIME_FORMAT_ISO8601_FULL);
    }

    _data = "{ data: '" + data + "', time: '" + fullTimeStr + "' }";
}

String EventInstance::GetEventName() {
    return _eventName;
}

String EventInstance::GetEventData() {
    return _data;
}

ParticleEventPublisher::ParticleEventPublisher() {
    _eventPrefix = "";
}

ParticleEventPublisher::ParticleEventPublisher(String eventPrefix) {
    _eventPrefix = eventPrefix;
}

void ParticleEventPublisher::Enqueue(String eventName, String data) {

    if (_eventQueue.size() >= MaxEventQueueSize) {
        _eventQueue.pop();
    }

    EventInstance instance = EventInstance(eventName, data);
    _eventQueue.push(instance);
}

void ParticleEventPublisher::Process() {
    if (_eventQueue.empty()) {
        return;
    }

    if (!_publishDelay.HasElapsed()) {
        return;
    }

    if (!WiFi.ready() || !Particle.connected()) {
        return;
    }

    EventInstance data = _eventQueue.front();

    String eventName;
    if (_eventPrefix == "") {
        eventName = data.GetEventName();
    }
    else {
        eventName = _eventPrefix + "/" + data.GetEventName();
    }

    bool success = Particle.publish(eventName, data.GetEventData());
    if (success) {
        _eventQueue.pop();
    }

    _publishDelay.Reset();
}

size_t ParticleEventPublisher::GetQueueLength() {
    return _eventQueue.size();
}
