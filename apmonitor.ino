#include "GrowLight.h"
#include "WaterLevelSensor.h"
#include "ParticleEventPublisher.h"
#include "PollingTimer.h"
#include "LightScheduler.h"
#include "RepeatingEvent.h"
#include "RetainedMem.h"
#include "ParticleConnection.h"

const String SketchVersion = "2.3.0";

// Notes about system configuration:
//
// 1. I've decided to disable the SYSTEM_THREAD functionality (or rather, to
//    stop enabling it). This is because I believe I've encountered system
//    thread deadlock and race condition issues related to connecting to weak
//    WiFi. Basically, if the Photon gets stuck connecting to WiFi at the wrong
//    time, then publishing an event or some other synchronous call on the user
//    thread will cause all user code to stop running. This is my theory at
//    least.
//
// 2. When in SEMI_AUTOMATIC mode we get two nice things:
//
//    a. Particle.connect() is non-blocking (I think)
//    b. User code starts running immediately.
//
//    This means we can function without WiFi, and not worry about
//    Particle.connect() halting the whole show on weak WiFi.
//
SYSTEM_MODE(SEMI_AUTOMATIC);

// Controls the lights. HIGH means "turn lights off" and LOW means "turn lights
// on"
const unsigned int RelayPin = 6;

// Determines water level. HIGH means water level is high. LOW means water
// level is low.
const unsigned int SumpTankPin = 5;

// If Photon has been disconnected from the Particle Cloud for longer than this
// time period, the Photon will reboot itself.
RepeatingEvent _rebootEvent = RepeatingEvent(rebootSystem, 86400000); // Reset device every 24 hours
RepeatingEvent _refreshRebootEvent = RepeatingEvent(refreshReboot, 10000); // Check if we need a reboot every 10 seconds.

// If Photon has not sent a sump water level status update for a longer period
// of time than this, the Photon will go ahead and send one.
RepeatingEvent _gratuitousWaterLevelEvent = RepeatingEvent(sendGratuitousWaterLevelUpdate, 14400000); // 4 hours

RepeatingEvent _waterLevelReadingEvent = RepeatingEvent(readWaterLevel, 1000); // Every second
WaterLevel _currentWaterLevel = Unknown; // Will be updated on first "loop" call
WaterLevelSensor _sumpWaterSensor = WaterLevelSensor(SumpTankPin);

ParticleEventPublisher _eventPublisher = ParticleEventPublisher("apsystem");

RepeatingEvent _growLightEvent = RepeatingEvent(updateGrowLight, 500); // Every half second
GrowLight _growLight = GrowLight(RelayPin, &_eventPublisher);
LightScheduler _lightScheduler = LightScheduler(&_growLight);

RepeatingEvent _persistTimeEvent = RepeatingEvent(saveOrRestoreTime, 60000); // Every minute
RepeatingEvent _publishStatusEvent = RepeatingEvent(publishStatus, 43200000); // Every 12 hours

ParticleConnection _particleConnection = ParticleConnection(&_eventPublisher);

RepeatingEvent _rssiRefreshEvent = RepeatingEvent(refreshRssi, 60000); // Every minute

String _particleGrowLightState = "On";
String _particleWaterLevel = "Unknown";
String _particleRssi = "0";

bool _resetPending = false;

void setup() {

    Time.zone(-6); // Central time

    _sumpWaterSensor.Init();
    _growLight.Init();

    Particle.function("lights", issueLightsCommand);
    Particle.function("reset", setPendingReset);
    Particle.variable("lights", _particleGrowLightState);
    Particle.variable("sump-level", _particleWaterLevel);
    Particle.variable("rssi", _particleRssi);

    _eventPublisher.Enqueue("AP01/version",
        "Sketch: " + SketchVersion + "; Firmware: " + System.version());
}

void loop() {

    _particleConnection.Process();
    _persistTimeEvent.Process();
    _waterLevelReadingEvent.Process();
    _growLightEvent.Process();
    _gratuitousWaterLevelEvent.Process();
    _eventPublisher.Process();
    _refreshRebootEvent.Process();
    _rebootEvent.Process();
    _publishStatusEvent.Process();
    _rssiRefreshEvent.Process();
}

void publishStatus() {

    String message = "EventQueueLength: " + String(_eventPublisher.GetQueueLength())
        + "; WiFiConnected: " + String(WiFi.ready())
        + "; ParticleConnected: " + String(Particle.connected())
        + "; WiFiRssi: " + String(WiFi.RSSI());
    
    _eventPublisher.Enqueue("AP01/status", message);
}

void refreshReboot() {

    if (_particleConnection.IsConnected()) {
        _rebootEvent.Reset(); // Everything's good. Prevent device from rebooting.
    }

    if (_resetPending) {
        rebootSystem();
    }
}

void rebootSystem() {
    saveOrRestoreTime();
    System.reset();
}

void updateGrowLight() {

    LightState lightStatus = _growLight.Process();

    if (lightStatus == On && _particleGrowLightState != "On") {
        _particleGrowLightState = "On";
    }
    else if (lightStatus == Off && _particleGrowLightState != "Off"){
        _particleGrowLightState = "Off";
    }

    _lightScheduler.Process();
}

void readWaterLevel() {

    WaterLevel waterLevelReading = _sumpWaterSensor.Read();

    if (waterLevelReading == AboveThreshold) {
        onHighWaterLevel();
    }
    else if (waterLevelReading == BelowThreshold) {
        onLowWaterLevel();
    }
    else {
        _particleWaterLevel = "Unknown";
        publishWaterLevelEvent("unknown");
    }
}

void onHighWaterLevel() {

    if (_currentWaterLevel != AboveThreshold) {
        _currentWaterLevel = AboveThreshold;
        _particleWaterLevel = "Above Threshold";
        publishWaterLevelEvent("above-threshold");
    }

}

void onLowWaterLevel() {

    if (_currentWaterLevel != BelowThreshold) {
        _currentWaterLevel = BelowThreshold;
        _particleWaterLevel = "Below Threshold";
        publishWaterLevelEvent("below-threshold");
    }

}

int issueLightsCommand(String command) {

    // This allows the user to control grow lights on a whim and will cause the
    // grow lights to turn on or off even if it's against the schedule.
    // Eventually the schedule will kick back in and override the user's
    // preference.

    if (command == "On" || command == "on" || command == "ON") {
        _growLight.TurnOn();
    }
    else if (command == "Off" || command == "off" || command == "OFF") {
        _growLight.TurnOff();
    }
    else {
        return -1;
    }

    return 0;
}

int setPendingReset(String command) {
    _resetPending = true;
    return 0;
}

void sendGratuitousWaterLevelUpdate() {

    // If no water level event has been sent for a long time, it's nice to just
    // say, "Hey, I'm still here, and the water level is still
    // high/low/unknown." This is useful for anyone recording water level
    // events, and allows listeners to know what the water level was recently
    // even if the Photon has been disconnected from the Internet for a while.

    if (_currentWaterLevel == AboveThreshold) {
        publishWaterLevelEvent("above-threshold");
    }
    else if (_currentWaterLevel == BelowThreshold) {
        publishWaterLevelEvent("below-threshold");
    }
    else if (_currentWaterLevel == Unknown) {
        publishWaterLevelEvent("unknown");
    }
    else {
        publishUnknownError("Invalid water level value: " + _currentWaterLevel);
    }
}

void publishWaterLevelEvent(String data) {
    _eventPublisher.Enqueue("sumpwaterlevel", data);

    // Don't need a gratuitous water level event for another 4 hours because we
    // just sent a water level event.
    _gratuitousWaterLevelEvent.Reset();
}

void publishUnknownError(String errorMsg) {
    _eventPublisher.Enqueue("error/unknown", errorMsg);
}

void saveOrRestoreTime() {

    time_t now = Time.now();
    if (now < SRAM_LastKnownTime && !Particle.connected()) {

        Time.setTime(SRAM_LastKnownTime);

        String debugMessage = "Changed time from "
            + Time.format(now, TIME_FORMAT_ISO8601_FULL)
            + " to "
            + Time.format(SRAM_LastKnownTime, TIME_FORMAT_ISO8601_FULL);
        
        // Since we're not connected, this event won't fire immediately. But it
        // eventually will once we do get reconnected. Note: I've not seen this
        // event in the wild. If we go long enough without seeing it, perhaps
        // we can kill the whole "time persistence" idea.
        _eventPublisher.Enqueue("debug/time/restored", debugMessage);
    }
    else {
        SRAM_LastKnownTime = now;
    }
}

void refreshRssi() {
    _particleRssi = String(WiFi.RSSI());
}
