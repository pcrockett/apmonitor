#include "LightScheduler.h"

LightScheduler::LightScheduler(GrowLight* lights) {
    _lights = lights;
    _currentStatus = lights->GetStatus();
}

void LightScheduler::Process() {

    if (_currentStatus == GetScheduledState()) {
        return;
    }

    // If we've gotten here, we know a change needs to be made.
    
    if (_currentStatus == Off) {
        _lights->TurnOn();
        _currentStatus = On;
    }
    else if (_currentStatus == On) {
        _lights->TurnOff();
        _currentStatus = Off;
    }
}

LightState LightScheduler::GetScheduledState() {
    // We may not have synced with the cloud yet, so the time could be wrong.
    // That's OK, we need this to work without Internet anyway.
    
    unsigned int hour = Time.hour(); // This is dependent on correct time zone settings.
    if (hour >= LightStartHour && hour < LightStopHour) {
        return On;
    }
    else {
        return Off;
    }
}
