#ifndef __APMONITOR_RETAINED_MEM_H
#define __APMONITOR_RETAINED_MEM_H

// Enable retained memory (SRAM). See:
// https://docs.particle.io/reference/firmware/photon/#storing-data-in-backup-ram-sram-
STARTUP(System.enableFeature(FEATURE_RETAINED_MEMORY));

// It is very important to not rearrange the order of variables in this file.
// Also don't insert new variables in between others. Always append new
// variables to the end. The exception for this rule is if you completely
// remove power from the Photon or do a hard reset. Then you can rearrange
// variables however you want.

// Allows us to know approximately what time it is when the Photon reboots but
// fails to reestablish an internet connection.
retained time_t SRAM_LastKnownTime = 0;

#endif
