#ifndef __APMONITOR_DELAY_TIMER_H
#define __APMONITOR_DELAY_TIMER_H

#include "application.h"

class PollingTimer {
public:
    PollingTimer(const unsigned long durationMillis);
    bool HasElapsed();
    void Reset();

private:
    unsigned long _lastResetTime;
    unsigned long _durationMillis;
};

#endif
