#ifndef __APMONITOR_WATER_LEVEL_SENSOR_H
#define __APMONITOR_WATER_LEVEL_SENSOR_H

#include "application.h"

enum WaterLevel {
    AboveThreshold = HIGH,
    BelowThreshold = LOW,
    Unknown = -1
};

class WaterLevelSensor {
public:
    WaterLevelSensor(const unsigned int sensorPin);
    void Init();
    WaterLevel Read();

private:
    unsigned int _pin;
};

#endif
