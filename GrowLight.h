#ifndef __APMONITOR_GROWLIGHT_H
#define __APMONITOR_GROWLIGHT_H

#include "application.h"
#include "PollingTimer.h"
#include "ParticleEventPublisher.h"

enum LightState {
    On = 1,
    Off = 0
};

// We don't want to switch the grow lights on and off rapidly. Minimum amount
// of time between changing grow lights state.
const unsigned long DelayMillis = 30000; // 30 seconds

class GrowLight {
public:
    GrowLight(const unsigned int relayPin, ParticleEventPublisher *eventPub);
    void Init();
    void TurnOn();
    void TurnOff();
    LightState GetStatus();
    LightState Process();

private:
    unsigned int _relayPin;
    LightState _currentLightState;
    LightState _desiredLightState;
    PollingTimer _changeDelay = PollingTimer(DelayMillis);
    ParticleEventPublisher* _eventPub;
};
#endif
